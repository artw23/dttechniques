import {Component} from '@angular/core';
import {LocalizeRouterService} from 'localize-router';
import {TranslateService} from '@ngx-translate/core';

declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
  constructor(private translate: TranslateService, private localize: LocalizeRouterService) {
    translate.addLangs(['en', 'es']);
    translate.setDefaultLang('es');
    translate.use('es');
    translate.get('HOME.Experiencia').subscribe((res: string) => {
      console.log(res);
    });
  }

  image = '';
  changeText = '';

  changeLanguage() {

    var language = this.localize.parser.currentLang;
    if (language == 'es') {
      this.localize.changeLanguage('en');
    } else if (language == 'en') {
      this.localize.changeLanguage('es');
    }
    this.update();
  }

  update() {
    console.log(this.localize);
    var language = this.localize.parser.currentLang;
    console.log(language);
    if (language == 'es') {
      this.image = '/assets/img/en.png';
      this.changeText = 'Switch to english';
    } else if (language == 'en') {
      this.image = '/assets/img/es.png';
      this.changeText = 'Cambiar a español';
    }
  }

  ngOnInit() {
    this.update();
  }
}
