import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor() {}

  public images = [
    'img-1',
    'img-2',
    'img-3',
    'img-4',
    'img-5',
    'img-6',
    'img-7'
  ];
  public actualImage = this.images[3];

  public counter = 0;

  updateImage(that) {
    that.counter++;
    that.actualImage = that.images[that.counter % 7];
  }

  ngOnInit() {
    setInterval(() => {
      this.updateImage(this);
    }, 2000);
  }
}
