import {Component, OnInit} from '@angular/core';
import 'rxjs/Rx';
import {Http, Response, Headers, RequestOptions} from "@angular/http";
import {TranslateService, LangChangeEvent} from '@ngx-translate/core';
import {LocalizeRouterService, LocalizeParser} from 'localize-router';
import {Router} from '@angular/router';

declare var window: any;


@Component({
  selector: 'app-examen-gdt',
  templateUrl: './examen-gdt.component.html',
  styleUrls: ['./examen-gdt.component.scss']
})
export class ExamenGdtComponent implements OnInit {

  private caracteristicas = ["Rectitud", "Planicidad", "Circularidad", "Cilindricidad",
    "Perfil de línea", "Perfil de una superficie", "Perpendicularidad", "Angularidad", "Paralelismo",
    "Oscilación circular", "Oscilación total", "Posición verdadera", "Concentricidad", "Simetría"
  ]

  private examen = {
    "nombre": "",
    "telefono": "",
    "correo": "",
    "q1": "",
    "q2": "",
    "q3": "",
    "q4_1": "",
    "q4_2": "",
    "q4_3": "",
    "q4_4": "",
    "q4_5": "",
    "q4_6": "",
    "q4_7": "",
    "q4_8": "",
    "q4_9": "",
    "q4_10": "",
    "q4_11": "",
    "q4_12": "",
    "q4_13": "",
    "q4_14": "",
    "q5": "",
    "q6": "",
    "q7": "",
    "q8": "",
    "q9": "",
    "q10": "",
    "q11": "",
    "q12": "",
    "q13": "",
    "q14": "",
    "q15": "",
    "q16": "",
    "correctas": 0,
    "incorrectas": 0,
    "total": 0
  }

  private respuestas = {
    "q1": 3,
    "q2": 4,
    "q3": 1,
    "q5": 1,
    "q6": 4,
    "q7": 1,
    "q9": 2,
    "q10": 4,
    "q11": 1,
    "q12": 2,
    "q13": 1,
    "q14": 3,
    "q15": 4,
    "q16": 2
  }

  public constructor(
    private http: Http,
    private router: Router,
    private translate: TranslateService,
    private localizeService: LocalizeRouterService,
    private parser: LocalizeParser
  ) {}

  ngOnInit() {
  }

  upload() {
    this.examen.correctas = 0;
    this.examen.incorrectas = 0;
    for (var property in this.respuestas) {
      if (this.respuestas[property] == this.examen[property]) {
        this.examen.correctas++;
      } else {
        this.examen[property] = this.examen[property] + "I" + this.respuestas[property];
        this.examen.incorrectas++;
      }
    }

    this.examen.total = this.examen.incorrectas + this.examen.correctas

    console.log(this.examen)
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers});
    let body = JSON.stringify(this.examen);
    this.http.post('assets/mail/sendExamenGdt.php', body, headers)
      .map(
      (res: Response) => res
      )
      .subscribe(
      err => this.logError(err),
      () => this.success()
      );
  }

  success2() {
    let translatedPath: any = this.localizeService.translateRoute('/mensaje-enviado');
    window.location = translatedPath;
  }

  success() {
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers});
    let body = JSON.stringify(this.examen);
    this.http.post('assets/mail/sendExamenEnviado.php', body, headers)
      .map(
      (res: Response) => res
      )
      .subscribe(
      err => this.logError2(err),
      () => this.success2()
      );
  }

  logError(err) {
    if (err.toString().indexOf("200 OK") !== -1) {
      this.success();
      return;
    }
    console.error('There was an error: ' + err);
  }

  logError2(err) {
    if (err.toString().indexOf("200 OK") !== -1) {
      this.success2();
      return;
    }
    console.error('There was an error: ' + err);
  }

}
