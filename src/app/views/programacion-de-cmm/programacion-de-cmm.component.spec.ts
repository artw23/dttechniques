/* tslint:disable:no-unused-variable */
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {DebugElement} from '@angular/core';

import {ProgramacionDeCmmComponent} from './programacion-de-cmm.component';

describe('ProgramacionDeCmmComponent', () => {
  let component: ProgramacionDeCmmComponent;
  let fixture: ComponentFixture<ProgramacionDeCmmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProgramacionDeCmmComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramacionDeCmmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
