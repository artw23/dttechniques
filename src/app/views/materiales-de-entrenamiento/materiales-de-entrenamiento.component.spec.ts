/* tslint:disable:no-unused-variable */
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {By} from '@angular/platform-browser';
import {DebugElement} from '@angular/core';

import {MaterialesDeEntrenamientoComponent} from './materiales-de-entrenamiento.component';

describe('MaterialesDeEntrenamientoComponent', () => {
  let component: MaterialesDeEntrenamientoComponent;
  let fixture: ComponentFixture<MaterialesDeEntrenamientoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MaterialesDeEntrenamientoComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialesDeEntrenamientoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
