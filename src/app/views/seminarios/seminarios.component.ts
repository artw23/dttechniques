import {Component, OnInit, ApplicationRef} from '@angular/core';
import {Contacto} from './contacto';
import 'rxjs/Rx';
import {Http, Response, Headers, RequestOptions} from "@angular/http";
import {TranslateService, LangChangeEvent} from '@ngx-translate/core';
import {LocalizeRouterService, LocalizeParser} from 'localize-router';
import {Router} from '@angular/router';

import {Seminario} from '../../services/seminarios/seminario';
import {SeminariosService} from '../../services/seminarios/seminarios.service';

import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';


declare var foundation: any;
declare var $: any;
declare var window: any;

@Component({
  selector: 'app-seminarios',
  templateUrl: './seminarios.component.html',
  styleUrls: ['./seminarios.component.scss']
})

export class SeminariosComponent implements OnInit {

  private subMes1 = ""
  private subMes2 = ""
  private mes1 = ""
  private mes2 = ""

  seminarios: Seminario[] = [];

  public contacto = new Contacto('', '', '', '', '', '');

  open(content) {
    this.modalService.open(content);
  }

  public constructor(
    private http: Http,
    private router: Router,
    private translate: TranslateService,
    private appRef: ApplicationRef,
    private localizeService: LocalizeRouterService,
    private parser: LocalizeParser,
    private seminariosService: SeminariosService,
    private modalService: NgbModal
  ) {}


  ngOnInit(): void {
    var lan = this.parser.currentLang;
    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      var lan = this.translate.currentLang;
      this.init(lan);
    });
    this.init(lan);
  }

  public init(lan) {
    var that = this;
    this.seminariosService.setLanguage(lan);
    this.seminariosService.getSeminarios()
      .then(function(response) {
        that.seminarios = that.chunk(response, 3);
      });
  }

  public chunk(arr, len) {

    var chunks = [],
      i = 0,
      n = arr.length;

    while (i < n) {
      chunks.push(arr.slice(i, i += len));
    }
    return chunks;
  }




  public submit() {
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers});
    let body = JSON.stringify(this.contacto);
    this.http.post('assets/mail/sendContact.php', body, headers)
      .map(
      (res: Response) => res
      )
      .subscribe(
      err => this.logError(err),
      () => this.success()
      );
  }

  success() {
    let translatedPath: any = this.localizeService.translateRoute('/mensaje-enviado');
    window.location = translatedPath;
  }

  logError(err) {
    if (err.toString().indexOf("200 OK") !== -1) {
      this.success();
      return;
    }
    console.error('There was an error: ' + err);
  }
}
