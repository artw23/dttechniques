export class Contacto {
  constructor(
    public nombre: string,
    public compania: string,
    public telefono: string,
    public sede: string,
    public correo: string,
    public seminario: string
  ) {}
}
