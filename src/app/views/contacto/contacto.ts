export class Contacto {
  constructor(
    public nombre: string,
    public empresa: string,
    public telefono: string,
    public correo: string,
    public mensaje: string
  ) {}
}
