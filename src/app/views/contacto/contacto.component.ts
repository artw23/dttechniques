import {Component, OnInit, ApplicationRef} from '@angular/core';
import {Contacto} from './contacto';
import {Http, Response, Headers, RequestOptions} from "@angular/http";
import {TranslateService, LangChangeEvent} from '@ngx-translate/core';
import {LocalizeRouterService, LocalizeParser} from 'localize-router';
import {Router} from '@angular/router';

import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';


declare var window: any;
declare var $: any;

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.scss']
})

export class ContactoComponent implements OnInit {

  public contacto = new Contacto('', '', '', '', '');


  public constructor(private http: Http,
    private router: Router,
    private translate: TranslateService,
    private appRef: ApplicationRef,
    private localizeService: LocalizeRouterService,
    private parser: LocalizeParser,
    private modalService: NgbModal
  ) {}


  ngOnInit() {
  }

  open(content) {
    this.modalService.open(content);
  }


  public submit() {
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers});
    let body = JSON.stringify(this.contacto);
    this.http.post('assets/mail/submitContacto.php', body, headers)
      .map(
      (res: Response) => res
      )
      .subscribe(
      err => this.logError(err),
      () => this.success()
      );
  }

  success() {
    let translatedPath: any = this.localizeService.translateRoute('/mensaje-enviado');
    window.location = translatedPath;
  }

  logError(err) {
    if (err.toString().indexOf("200 OK") !== -1) {
      this.success();
      return;
    }
    console.error('There was an error: ' + err);
  }

  lat: number = 35.886516;
  lng: number = -89.278384;
  zoom: number = 4;


  markers: marker[] = [
    {
      lat: 20.597340,
      lng: -100.402323,
      label: 'Queretaro',
      draggable: false
    },
    {
      lat: 19.041595,
      lng: -98.207314,
      label: 'Puebla',
      draggable: false
    },
    {
      lat: 25.699888,
      lng: -100.329693,
      label: 'Monterrey',
      draggable: false
    },
    {
      lat: 42.699888,
      lng: -88.329693,
      label: 'USA1',
      draggable: false
    },
    {
      lat: 42.699888,
      lng: -85.329693,
      label: 'USA2',
      draggable: false
    },

  ]


}

interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}
