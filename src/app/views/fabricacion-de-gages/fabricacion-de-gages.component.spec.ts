/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { FabricacionDeGagesComponent } from './fabricacion-de-gages.component';

describe('FabricacionDeGagesComponent', () => {
  let component: FabricacionDeGagesComponent;
  let fixture: ComponentFixture<FabricacionDeGagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FabricacionDeGagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FabricacionDeGagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
