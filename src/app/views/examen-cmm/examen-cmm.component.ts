import {Component, OnInit} from '@angular/core';
import 'rxjs/Rx';
import {Http, Response, Headers, RequestOptions} from "@angular/http";
import {TranslateService, LangChangeEvent} from '@ngx-translate/core';
import {LocalizeRouterService, LocalizeParser} from 'localize-router';
import {Router} from '@angular/router';

declare var window: any;


@Component({
  selector: 'app-examen-cmm',
  templateUrl: './examen-cmm.component.html',
  styleUrls: ['./examen-cmm.component.scss']
})
export class ExamenCmmComponent implements OnInit {

  private examen = {
    "nombre": "",
    "telefono": "",
    "email": "",
    "q1": "",
    "q2": "",
    "q3": "",
    "q4": "",
    "q5": "",
    "q6": "",
    "q7": "",
    "q8": "",
    "q9": "",
    "q10": "",
    "q11": "",
    "q12": "",
    "q13": "",
    "q14": "",
    "correctas": 0,
    "incorrectas": 0,
    "total": 0
  }

  private respuestas = {
    "q1": 2,
    "q3": 4,
    "q4": 3,
    "q9": 1,
    "q11": 4,
    "q14": 1
  }

  public constructor(
    private http: Http,
    private router: Router,
    private translate: TranslateService,
    private localizeService: LocalizeRouterService,
    private parser: LocalizeParser
  ) {}

  ngOnInit() {
  }

  upload() {
    this.examen.correctas = 0;
    this.examen.incorrectas = 0;
    for (var property in this.respuestas) {
      if (this.respuestas[property] == this.examen[property]) {
        this.examen.correctas++;
      } else {
        this.examen[property] = this.examen[property] + "I" + this.respuestas[property];
        this.examen.incorrectas++;
      }
    }

    this.examen.total = this.examen.incorrectas + this.examen.correctas

    console.log(this.examen)
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers});
    let body = JSON.stringify(this.examen);
    this.http.post('assets/mail/sendExamenCmm.php', body, headers)
      .map(
      (res: Response) => res
      )
      .subscribe(
      err => this.logError(err),
      () => this.success()
      );
  }

  success2() {
    let translatedPath: any = this.localizeService.translateRoute('/mensaje-enviado');
    window.location = translatedPath;
  }

  success() {
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers});
    let body = JSON.stringify(this.examen);
    this.http.post('assets/mail/sendExamenEnviado.php', body, headers)
      .map(
      (res: Response) => res
      )
      .subscribe(
      err => this.logError2(err),
      () => this.success2()
      );
  }

  logError(err) {
    if (err.toString().indexOf("200 OK") !== -1) {
      this.success();
      return;
    }
    console.error('There was an error: ' + err);
  }

  logError2(err) {
    if (err.toString().indexOf("200 OK") !== -1) {
      this.success2();
      return;
    }
    console.error('There was an error: ' + err);
  }
}
