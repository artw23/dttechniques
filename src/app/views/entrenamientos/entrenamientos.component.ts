import {Component, OnInit} from '@angular/core';
import {LocalizeRouterService, LocalizeParser} from 'localize-router';
import {Http, Response, Headers, RequestOptions} from "@angular/http";
import {TranslateService, LangChangeEvent} from '@ngx-translate/core';
import {Contacto} from './contacto';

import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';


import {Entrenamiento} from '../../services/entrenamientos/entrenamiento';
import {EntrenamientosService} from '../../services/entrenamientos/entrenamientos.service';

declare var $: any;
declare var window: any;

@Component({
  selector: 'app-entrenamientos',
  templateUrl: './entrenamientos.component.html',
  styleUrls: ['./entrenamientos.component.scss']
})
export class EntrenamientosComponent implements OnInit {

  public contacto = new Contacto('', '', '', '', '');


  private lan = '';
  entrenamientos: Entrenamiento[] = [];
  private displayContacto = false;
  actualEntrenamiento = Entrenamiento;

  open(content) {
    this.modalService.open(content);
  }

  setActual(entrenamiento, content) {
    this.displayContacto = false;
    this.actualEntrenamiento = entrenamiento;
    this.modalService.open(content);
  }

  constructor(
    private localize: LocalizeRouterService,
    private parser: LocalizeParser,
    private translate: TranslateService,
    private localizeService: LocalizeRouterService,
    private entrenamientosService: EntrenamientosService,
    private http: Http,
    private modalService: NgbModal
  ) {}

  ngOnInit() {
    var lan = this.parser.currentLang;

    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      var lan = this.translate.currentLang;
      this.init(lan);
    });
    this.init(lan);
  }

  public init(lan) {
    var that = this;
    this.entrenamientosService.setLanguage(lan);
    this.entrenamientosService.getEntrenamientos()
      .then(function(response) {
        console.log(response)
        that.entrenamientos = that.chunk(response, 3);
      });
  }

  public chunk(arr, len) {

    var chunks = [],
      i = 0,
      n = arr.length;

    while (i < n) {
      chunks.push(arr.slice(i, i += len));
    }
    return chunks;
  }


  public submit() {
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers});
    let body = JSON.stringify(this.contacto);
    this.http.post('assets/mail/submitContacto.php', body, headers)
      .map(
      (res: Response) => res
      )
      .subscribe(
      err => this.logError(err),
      () => this.success()
      );
  }

  success() {
    let translatedPath: any = this.localizeService.translateRoute('/mensaje-enviado');
    window.location = translatedPath;
  }

  logError(err) {
    if (err.toString().indexOf("200 OK") !== -1) {
      this.success();
      return;
    }
    console.error('There was an error: ' + err);
  }

}
