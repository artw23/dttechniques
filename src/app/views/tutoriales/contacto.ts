export class Contacto {
  constructor(
    public nombre: string,
    public telefono: string,
    public correo: string,
  ) {}
}
