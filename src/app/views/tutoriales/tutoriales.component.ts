import {Component, OnInit} from '@angular/core';
import {Contacto} from './contacto';
import {TranslateService, LangChangeEvent} from '@ngx-translate/core';
import {LocalizeRouterService, LocalizeParser} from 'localize-router';

declare var window: any;
declare var $: any;

@Component({
  selector: 'app-tutoriales',
  templateUrl: './tutoriales.component.html',
  styleUrls: ['./tutoriales.component.scss']
})
export class TutorialesComponent implements OnInit {

  private lan = '';
  constructor(
    private localizeService: LocalizeRouterService,
    private parser: LocalizeParser,
    private translate: TranslateService,
  ) {}

  filesToUpload: File;
  files: Array<File> = [];
  public edited = false;
  public nombre;
  public telefono;
  public correo;

  ngOnInit() {
    this.lan = this.parser.currentLang;
    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.lan = this.translate.currentLang;
    });
  }

  upload() {
    this.makeFileRequest("assets/mail/submitExam.php", [], this.filesToUpload).then((result) => {
      console.log(result);
      location.reload();
    }, (error) => {
      console.error(error);
    });
  }

  success() {
    console.log("SUCESS");
    let translatedPath: any = this.localizeService.translateRoute('/mensaje-enviado');
    window.location = translatedPath;
  }

  fileChangeEvent(fileInput: any) {
    this.edited = true;
    this.files = <Array<File>>fileInput.target.files;
    this.filesToUpload = this.files[0];
  }

  makeFileRequest(url: string, params: Array<string>, file: File) {
    var that = this;
    return new Promise((resolve, reject) => {
      var formData: any = new FormData();
      var xhr = new XMLHttpRequest();
      formData.append("examen", file, file.name);
      formData.append("nombre", this.nombre);
      formData.append("correo", this.correo);
      formData.append("telefono", this.telefono);
      xhr.onreadystatechange = function() {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            that.success();
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }
}
