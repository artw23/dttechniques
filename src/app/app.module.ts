// Angular imports
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import {HttpModule} from '@angular/http';
import {Location} from '@angular/common';

// Bootstrap component
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

// ngx translator
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

// localize router
import {LocalizeRouterModule} from 'localize-router';
import {LocalizeRouterHttpLoader} from 'localize-router-http-loader';

import {RoutesPath, AppRoutingModule} from './app-routing.module';

// AGM
import {AgmCoreModule} from '@agm/core';

// Site components
import {AppComponent} from './app.component';
import {TutorialesComponent} from './views/tutoriales/tutoriales.component';
import {MensajeEnviadoComponent} from './views/mensaje-enviado/mensaje-enviado.component';
import {SoporteEnSitioComponent} from './views/soporte-en-sitio/soporte-en-sitio.component';
import {SomosComponent} from './views/somos/somos.component';
import {SeminariosComponent} from './views/seminarios/seminarios.component';
import {ProgramacionDeCmmComponent} from './views/programacion-de-cmm/programacion-de-cmm.component';
import {ProductosComponent} from './views/productos/productos.component';
import {MaterialesDeEntrenamientoComponent} from './views/materiales-de-entrenamiento/materiales-de-entrenamiento.component';
import {HomeComponent} from './views/home/home.component';
import {EntrenamientosComponent} from './views/entrenamientos/entrenamientos.component';
import {ContactoComponent} from './views/contacto/contacto.component';
import {AccesoriosComponent} from './views/accesorios/accesorios.component';
import {SeminariosService} from './services/seminarios/seminarios.service';
import {EntrenamientosService} from './services/entrenamientos/entrenamientos.service';
import {ExamenGdtComponent} from './views/examen-gdt/examen-gdt.component';
import {ExamenCmmComponent} from './views/examen-cmm/examen-cmm.component';
import {ExamenEnviadoComponent} from './views/examen-enviado/examen-enviado.component';
import {FabricacionDeGagesComponent} from './views/fabricacion-de-gages/fabricacion-de-gages.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, '/assets/locales/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    TutorialesComponent,
    MensajeEnviadoComponent,
    SoporteEnSitioComponent,
    SomosComponent,
    SeminariosComponent,
    ProgramacionDeCmmComponent,
    ProductosComponent,
    MaterialesDeEntrenamientoComponent,
    HomeComponent,
    EntrenamientosComponent,
    ContactoComponent,
    AccesoriosComponent,
    ExamenGdtComponent,
    ExamenCmmComponent,
    ExamenEnviadoComponent,
    FabricacionDeGagesComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    AppRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyA_RatKERNuy_fSaiYZ55U12ELwpkQ3W9k'
    }),
    NgbModule.forRoot()

  ],
  providers: [
    SeminariosService,
    EntrenamientosService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
