import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LocalizeRouterModule, LocalizeRouterSettings, LocalizeParser} from 'localize-router';
import {LocalizeRouterHttpLoader} from 'localize-router-http-loader';
import {TranslateService} from '@ngx-translate/core';
import {Location} from '@angular/common';
import {HttpClient} from '@angular/common/http';


import {TutorialesComponent} from './views/tutoriales/tutoriales.component';
import {MensajeEnviadoComponent} from './views/mensaje-enviado/mensaje-enviado.component';
import {SoporteEnSitioComponent} from './views/soporte-en-sitio/soporte-en-sitio.component';
import {SomosComponent} from './views/somos/somos.component';
import {SeminariosComponent} from './views/seminarios/seminarios.component';
import {ProgramacionDeCmmComponent} from './views/programacion-de-cmm/programacion-de-cmm.component';
import {ProductosComponent} from './views/productos/productos.component';
import {MaterialesDeEntrenamientoComponent} from './views/materiales-de-entrenamiento/materiales-de-entrenamiento.component';
import {HomeComponent} from './views/home/home.component';
import {EntrenamientosComponent} from './views/entrenamientos/entrenamientos.component';
import {ContactoComponent} from './views/contacto/contacto.component';
import {AccesoriosComponent} from './views/accesorios/accesorios.component';
import {ExamenGdtComponent} from './views/examen-gdt/examen-gdt.component';
import {ExamenCmmComponent} from './views/examen-cmm/examen-cmm.component';
import {ExamenEnviadoComponent} from './views/examen-enviado/examen-enviado.component';
import {FabricacionDeGagesComponent} from './views/fabricacion-de-gages/fabricacion-de-gages.component';

export function HttpLoaderFactory(translate: TranslateService, location: Location, settings: LocalizeRouterSettings, http: HttpClient) {
  return new LocalizeRouterHttpLoader(translate, location, settings, http);
}

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'somos',
    component: SomosComponent
  },
  {
    path: 'contacto',
    component: ContactoComponent
  },
  {
    path: 'productos',
    component: ProductosComponent
  },
  {
    path: 'productos/programacion-de-cmm',
    component: ProgramacionDeCmmComponent
  },
  {
    path: 'productos/soporte-en-sitio-y-remoto',
    component: SoporteEnSitioComponent
  },
  {
    path: 'productos/entrenamientos',
    component: EntrenamientosComponent
  },
  {
    path: 'productos/materiales-de-entrenamiento',
    component: MaterialesDeEntrenamientoComponent
  },
  {
    path: 'productos/accesorios-mantenimiento-y-calibracion-de-cmm',
    component: AccesoriosComponent
  },
  {
    path: 'tutoriales',
    component: TutorialesComponent
  },
  {
    path: 'seminarios-publicos',
    component: SeminariosComponent
  },
  {
    path: 'mensaje-enviado',
    component: MensajeEnviadoComponent
  },
  {
    path: 'examen/gdt',
    component: ExamenGdtComponent
  },
  {
    path: 'examen/cmm',
    component: ExamenCmmComponent
  },
  {
    path: 'examen-enviado',
    component: ExamenEnviadoComponent
  },
  {
    path: 'productos/fabricacion-de-gages-funcionales',
    component: FabricacionDeGagesComponent
  }

];

export const RoutesPath: Routes = routes;

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    LocalizeRouterModule.forRoot(routes, {
      parser: {
        provide: LocalizeParser,
        useFactory: HttpLoaderFactory,
        deps: [TranslateService, Location, LocalizeRouterSettings, HttpClient]
      }
    })
  ],
  exports: [RouterModule, LocalizeRouterModule]
})


export class AppRoutingModule {
}


/*
Copyright 2016 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
