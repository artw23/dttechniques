/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SeminariosService } from './seminarios.service';

describe('SeminariosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SeminariosService]
    });
  });

  it('should ...', inject([SeminariosService], (service: SeminariosService) => {
    expect(service).toBeTruthy();
  }));
});
