export class Seminario {
  id: number;
  titulo: string;
  imagen: string;
  tipo: string;
  fecha: string;
  lugar: string;
}
