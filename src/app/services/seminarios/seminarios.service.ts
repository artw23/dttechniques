import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Seminario } from './seminario';

@Injectable()
export class SeminariosService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private url = 'assets/data/en/seminarios.json';

  public setLanguage(lan){
    this.url = 'assets/data/'+lan+'/seminarios.json';
    console.log(this.url)
  }

  constructor(private http: Http) { }

  getSeminarios(): Promise<Seminario[]> {
    return this.http.get(this.url)
               .toPromise()
               .then(
                 response => response.json() as Seminario[]
               )
               .catch(this.handleError);
  }


  getSeminario(id: number): Promise<Seminario> {
    const url = `${this.url}/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json().data as Seminario)
      .catch(this.handleError);
  }

  delete(id: number): Promise<void> {
    const url = `${this.url}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  create(name: string): Promise<Seminario> {
    return this.http
      .post(this.url, JSON.stringify({name: name}), {headers: this.headers})
      .toPromise()
      .then(res => res.json().data)
      .catch(this.handleError);
  }

  update(seminario: Seminario): Promise<Seminario> {
    const url = `${this.url}/${seminario.id}`;
    return this.http
      .put(url, JSON.stringify(seminario), {headers: this.headers})
      .toPromise()
      .then(() => seminario)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

}
