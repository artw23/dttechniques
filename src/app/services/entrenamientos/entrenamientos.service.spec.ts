/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { EntrenamientosService } from './entrenamientos.service';

describe('EntrenamientosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EntrenamientosService]
    });
  });

  it('should ...', inject([EntrenamientosService], (service: EntrenamientosService) => {
    expect(service).toBeTruthy();
  }));
});
