import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Entrenamiento } from './entrenamiento';

@Injectable()
export class EntrenamientosService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private url = 'assets/data/en/entrenamientos.json';

  public setLanguage(lan){
    this.url = 'assets/data/'+lan+'/entrenamientos.json';
    console.log(this.url)
  }

  constructor(private http: Http) { }

  getEntrenamientos(): Promise<Entrenamiento[]> {
    return this.http.get(this.url)
               .toPromise()
               .then(
                 response => response.json() as Entrenamiento[]
               )
               .catch(this.handleError);
  }


  getEntrenamiento(id: number): Promise<Entrenamiento> {
    const url = `${this.url}/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json().data as Entrenamiento)
      .catch(this.handleError);
  }

  delete(id: number): Promise<void> {
    const url = `${this.url}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  create(name: string): Promise<Entrenamiento> {
    return this.http
      .post(this.url, JSON.stringify({name: name}), {headers: this.headers})
      .toPromise()
      .then(res => res.json().data)
      .catch(this.handleError);
  }

  update(Entrenamiento: Entrenamiento): Promise<Entrenamiento> {
    const url = `${this.url}/${Entrenamiento.id}`;
    return this.http
      .put(url, JSON.stringify(Entrenamiento), {headers: this.headers})
      .toPromise()
      .then(() => Entrenamiento)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

}
