export class Entrenamiento {
  id: number;
  imagen: string;
  titulo: string;
  descripcion: string;
  link: string;
}
