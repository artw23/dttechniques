<?php

function replaceAnswer($text, $value, $message){
  if (strpos($value,'I') !== false) {
    $userValue = split('I',$value)[0];
    if($userValue > 0 && $userValue <= 4){
      $toReplace = "%".$text.".".$userValue."%\" style=\"";
      $message = str_replace($toReplace, "\" style=\"color:#D8000C; ", $message);
    }
  }else{
    $toReplace = "%".$text.".".$value."%\" style=\"";
    echo $toReplace;
    $message = str_replace($toReplace, "\" style=\"color:#4F8A10; ",$message);
  }
  return $message;
}

/**
 * This example shows settings to use when sending via Google's Gmail servers.
 */
//SMTP needs accurate times, and the PHP time zone MUST be set
//This should be done in your php.ini, but this is how to do it if you don't have access to that
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

date_default_timezone_set('Etc/UTC');
require 'mail/PHPMailerAutoload.php';
//Create a new PHPMailer instance
$mail = new PHPMailer;
//Tell PHPMailer to use SMTP
//$mail->isSMTP();
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 2;
//Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';
//Set the hostname of the mail server
$mail->Host = 'smtp.gmail.com';
// use
// $mail->Host = gethostbyname('smtp.gmail.com');
// if your network does not support SMTP over IPv6
//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = 587;
//Set the encryption system to use - ssl (deprecated) or tls
$mail->SMTPSecure = 'tsl';
//Whether to use SMTP authentication
$mail->SMTPAuth = true;
//Username to use for SMTP authentication - use full email address for gmail
$mail->Username = "webpage.imt@gmail.com";
//Password to use for SMTP authentication
$mail->Password = "imt1974!";
$mail->CharSet = "UTF-8";
//Set who the message is to be sent from
$mail->setFrom('contacto-web@imttechniques.com', 'Contacto sitio web');
//Set who the message is to be sent to
$mail->addAddress('webpage.imt@gmail.com', 'Ventas imttechniques');
//$mail->addAddress('artw23@gmail.com', 'Ventas imttechniques');
//Set the subject line
$mail->Subject = 'Contacto sitio web';
//Replace the plain text body with one created manually
$message = file_get_contents('./templateExamenGdt.html');
$message = str_replace('%nombre%', $request->nombre, $message);
$message = str_replace('%email%', $request->email, $message);
$message = str_replace('%telefono%', $request->telefono, $message);
$message = str_replace('%correctas%', $request->correctas, $message);
$message = str_replace('%total%', $request->total, $message);

$message = replaceAnswer('Q1', $request->q1, $message);
$message = replaceAnswer('Q2', $request->q2, $message);
$message = replaceAnswer('Q3', $request->q3, $message);
$message = replaceAnswer('Q4', $request->q4, $message);
$message = replaceAnswer('Q5', $request->q5, $message);
$message = replaceAnswer('Q6', $request->q6, $message);
$message = replaceAnswer('Q7', $request->q7, $message);
$message = str_replace('%Q8%', $request->total, $message);
$message = replaceAnswer('Q9', $request->q9, $message);
$message = replaceAnswer('Q10', $request->q10, $message);
$message = replaceAnswer('Q11', $request->q11, $message);
$message = replaceAnswer('Q12', $request->q12, $message);
$message = replaceAnswer('Q13', $request->q13, $message);
$message = replaceAnswer('Q14', $request->q14, $message);
$message = replaceAnswer('Q15', $request->q15, $message);
$message = replaceAnswer('Q16', $request->q16, $message);

$mail->MsgHTML($message);
//send the message, check for errors
if (!$mail->send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
} else {
    echo "Message sent!";
}
