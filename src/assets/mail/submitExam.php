<?php
/**
 * This example shows settings to use when sending via Google's Gmail servers.
 */
//SMTP needs accurate times, and the PHP time zone MUST be set
//This should be done in your php.ini, but this is how to do it if you don't have access to that
function redirect($url, $statusCode = 303)
{
   header('Location: ' . $url, true, $statusCode);
   die();
}


$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

date_default_timezone_set('Etc/UTC');
require 'mail/PHPMailerAutoload.php';
//Create a new PHPMailer instance
$mail = new PHPMailer;
//Tell PHPMailer to use SMTP
//$mail->isSMTP();
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 2;
//Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';
//Set the hostname of the mail server
$mail->Host = 'smtp.gmail.com';
$mail->CharSet = "UTF-8";
// use
// $mail->Host = gethostbyname('smtp.gmail.com');
// if your network does not support SMTP over IPv6
//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = 587;
//Set the encryption system to use - ssl (deprecated) or tls
$mail->SMTPSecure = 'tsl';
//Whether to use SMTP authentication
$mail->SMTPAuth = true;
//Username to use for SMTP authentication - use full email address for gmail
$mail->Username = "webpage.imt@gmail.com";
//Password to use for SMTP authentication
$mail->Password = "imt1974!";

if (isset($_FILES['examen']) &&
    $_FILES['examen']['error'] == UPLOAD_ERR_OK) {
    $mail->AddAttachment($_FILES['examen']['tmp_name'],
                         $_FILES['examen']['name']);
}

//Set who the message is to be sent from
$mail->setFrom('contacto-web@imttechniques.com', 'Contacto sitio web');
//Set who the message is to be sent to
$mail->addAddress('webpage.imt@gmail.com', 'Ventas imttechniques');
//$mail->addAddress('artw23@gmail.com', 'Ventas imttechniques');
//Set the subject line
$mail->Subject = 'Resultados del examen';
//Replace the plain text body with one created manually
$mail->Body   = 'Examen enviado via sitio web<br>'.
                '<b>Nombre:</b> ' . $_POST['nombre'] . '<br>'.
                '<b>Correo:</b> ' . $_POST['correo'] . '<br>'.
                '<b>Teléfono:</b> ' . $_POST['telefono'] . '<br>';
$mail->IsHTML(true);
//send the message, check for errors
if (!$mail->send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
} else {
    echo "Message sent!";
}
